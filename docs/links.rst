Links
=====

* Source code: https://bitbucket.org/qecsim/qecsim/
* Documentation: https://davidtuckett.com/qit/qecsim/
* Issue tracker: https://bitbucket.org/qecsim/qecsim/issues
* Releases: https://pypi.org/project/qecsim/
* Contact: qecsim@gmail.com
